<?php
namespace App\Library\Services;

use App\Category;

class CategoryService
{

    public $fmtCategories = [];
    public $fmtCategoriesForTable = [];


    public function getStructuredCategories($appId)
    {
        $categories =  Category::where('application_id', $appId)->orderBy('p_id','ASC')->get();
        $categories = $this->toSortByParentId($categories);
        //dd($categories);
        for ($i = 0; $i < count($categories); $i++) {
          $result = [[], false];
          $obj =  json_decode(json_encode($categories[$i]), true);
          $result = $this->categoriesFormat($this->fmtCategories,  $obj);
          if ($result[1] == 0) {

            $obj['title'] = $obj['name'];
            array_push($this->fmtCategories,  $obj);
          } else
            $this->fmtCategories = $result[0];
        }
        return  $this->fmtCategories;
    }



    private function categoriesFormat($arr, $obj)
    {
        $flag = 0;
        for ($i = 0; $i < count($arr); $i++) {

            if ($arr[$i]['id'] == $obj['p_id']) {
                if (!isset($arr[$i]['subs'])) {
                  $arr[$i]['subs'] = array();
                }
                $obj['title'] = $obj['name'];
                array_push($arr[$i]['subs'],  $obj);
                $flag = 1;
                return [$arr, $flag];
            } else if (isset($arr[$i]['subs'])) {
                $result =  $this->categoriesFormat($arr[$i]['subs'], $obj);
                if ($result[1] == 1) {
                  $arr[$i]['subs'] = $result[0];
                  $flag = $result[1];
                }
            }
        }
        return [$arr, $flag];
    }

    private function toSortByParentId($arr){

        $returnArr = [];

        for($i =0; $i< count($arr); $i++){

            $flagId = 0;
            for($j =$i+1; $j< count($arr); $j++){             
                  
                  if( $arr[$i]['p_id'] ==  $arr[$j]['id'] ){
                      $flagId = $j;                    
                      break;
                  }           

            }     
            if($flagId > 0){

                array_push($returnArr,  $arr[$flagId]);
                array_push($returnArr,  $arr[$i]);
                
                $temp = [];
                for($k =0; $k< count($arr); $k++){
                  if($flagId != $k)
                    array_push($temp,  $arr[$k]);
                }
                $arr = $temp;
            }else{
              array_push($returnArr,  $arr[$i]);
            }
            
        }

        return $returnArr; 
    }




    public function categoriesFormatForTable($arr,  $parent_id, $child_id )
    {
        for ($i = 0; $i < count($arr); $i++) {

            $child_id = $parent_id == '' ? ($i + 1) : $parent_id . '-' . ($i + 1);
            $obj = ['child_id' => $child_id, 'parent_id' => $parent_id, 'id' => $arr[$i]['id'], 'name' => $arr[$i]['name'], 'img_name' => $arr[$i]['img_name'], 'p_id' => $arr[$i]['p_id'], 'created_at'=>$arr[$i]['created_at']];

            array_push($this->fmtCategoriesForTable,  $obj);

            if (isset($arr[$i]['subs'])) {
                $temp_parent = $parent_id ;
                $parent_id = $child_id;
                $this->categoriesFormatForTable($arr[$i]['subs'], $parent_id, $child_id );
                $parent_id  = $temp_parent;
            }
        }
        return $this->fmtCategoriesForTable;
        
    }
}
