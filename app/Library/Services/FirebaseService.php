<?php
namespace App\Library\Services;

use App\Configs;

class FirebaseService
{

    public function pushNotifications($title, $type, $body, $imagePath)
    {

        $serverKey = Configs::where('application_id', session()->get('application')['id'])
            ->where('key', 'server-key')
            ->first();

        $url = "https://fcm.googleapis.com/fcm/send";
        $topico = "general";
        $server_key = $serverKey->value;

        $headers = array(
            'Authorization: key=' . $server_key,
            'Content-Type: application/json;charset=UTF-8'
        );

        $data = array(
            'data' =>
            array(
                'title' => $title,
                'type' => $type,
                'body' => $body,
                'imagePath' => $imagePath
            ),
            'to' => '/topics/' . $topico,
            'priority' => 'high',
            //'restricted_package_name' => 'com.onlyoneapp.test', //IF YOU WANT SEND TO ONLY ONE APP
        );

        //dd($data);


        $content = json_encode($data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);
        $arr = array();
        $arr = json_decode($result, true);
        //dd($arr);
        if ($arr === FALSE) {
            return "JSON Invalid";
        } else if (empty($arr)) {
            return "JSON Invalid";
        } else {
            if (array_key_exists('message_id', $arr)) {
                return "Notification send. Message id: " . $arr['message_id'];
            } else {
                return "Message id not found";
            }
        }
    }
}
