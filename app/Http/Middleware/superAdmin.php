<?php

namespace App\Http\Middleware;

use Closure;

class superAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role == 'super_admin'){
            if(session()->has('application')){
                return redirect()->route('dashboard');
            }
            return $next($request);
        }
        return redirect()->route('dashboard');
        
    }
}
