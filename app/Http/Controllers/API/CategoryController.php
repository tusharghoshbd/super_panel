<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Http\Controllers\API\Controller as Controller;
use App\Library\Services\CategoryService;
use App\Category;
use Validator;


class CategoryController extends Controller
{

    //public $apiFmtCategories = [];

    // public function index()
    // {        
    //      $catagories = Category::all();
    //      return $this->sendResponse($catagories->toArray(), 'Categories retrieved successfully.');
    // }

    /*
    * 
    * localhost:8000/api/category-list
    * Mthoed: POST    
    * Body:{ "app_id": 1 }
    * 
    */
    public function categoryList(Request $request){

        $body  = $request->all();
        $categoryService = new CategoryService ();
        $categories = $categoryService->getStructuredCategories( $body['app_id'] );
        $categoriesFormated  = $this->categoryListFormat($categories);
        if (is_null($categories)) {
            return $this->sendError('Category not found.');
        }
        return $this->sendResponse($categoriesFormated, 'Category retrieved successfully.');
    }








    private function categoryListFormat($arr){

        $apiFmtCategories =[];

        for ($i = 0; $i < count($arr); $i++) {
            
                $obj = [
                        'id' => $arr[$i]['id'], 
                        'title' => $arr[$i]['name'], 
                        'image_url' => url('uploads/categories/'.$arr[$i]['img_name'] ),
                        'parent_id' => $arr[$i]['p_id'], 
                        'has_child' => isset($arr[$i]['subs']) ? true : false, 
                        'modified_date' => isset($arr[$i]["updated_at"])&&!empty($arr[$i]["updated_at"]) ? Carbon::parse($arr[$i]["created_at"])->format('d-m-Y') : Carbon::parse($arr[$i]["updated_at"])->format('d-m-Y') , 
                        'sub' => []
                    ];            

                array_push($apiFmtCategories,  $obj);
            

            if (isset($arr[$i]['subs'])) {
                $apiFmtCategories[$i]['sub'] =  $this->categoryListFormat($arr[$i]['subs']);                
            }

        }
        return $apiFmtCategories;
    }



}