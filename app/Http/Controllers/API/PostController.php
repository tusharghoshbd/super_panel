<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Http\Controllers\API\Controller as Controller;
use App\Category;
use App\Post;
use Validator;
use App\PostCategories;
use App\PostImages;
use App\AdditionalFields;
use Mockery\Undefined;
use DB;


class PostController extends Controller
{

    
    /*
    * 
    * localhost:8000/api/post-list
    * Mthoed: POST    
    * Body:{
                "app_id": 1, (Mandatory fields)
                "post_id": 1 (optional fiels) -get specific post
            }
    * 
    */
    public function postList(Request $request){

        $body  = $request->all();
        $posts =  Post::where('application_id',$body['app_id']);
                    if(isset($body['post_id']))  
                        $posts->where('posts.id', $body['post_id'])  ;
        $posts =    $posts->orderBy('id','DESC')->get();

        $postImages = PostImages::select('post_images.*')
                    ->join('posts', 'posts.id', '=', 'post_images.post_id')
                    ->where('posts.application_id', $body['app_id']);
                        if(isset($body['post_id']))  
                            $postImages->where('post_images.post_id', $body['post_id'])  ;
        $postImages = $postImages->get();

        $additionalFields = AdditionalFields::select('additional_fields.*')
                ->join('posts', 'posts.id', '=', 'additional_fields.post_id')
                ->where('posts.application_id', $body['app_id']);
                if(isset($body['post_id']))  
                    $additionalFields->where('additional_fields.post_id', $body['post_id'])  ;
        $additionalFields = $additionalFields->get();

        $postCategories = PostCategories::select('categories.*','post_categories.post_id as post_id')
                ->join('posts', 'posts.id', '=', 'post_categories.post_id')
                ->join('categories', 'categories.id', '=', 'post_categories.category_id')
                ->where('posts.application_id', $body['app_id']);
                if(isset($body['post_id']))  
                    $postCategories->where('post_categories.post_id', $body['post_id'])  ;
        $postCategories = $postCategories->get();
               
        $formatedPost = $this->postListFormate($posts, $postImages, $additionalFields, $postCategories);

        if (is_null($posts)) {
            return $this->sendError('Category not found.');
        }

        return $this->sendResponse($formatedPost, 'Post retrieved successfully.');
    }


    /*
    * 
    * localhost:8000/api/post-list-by-category-id
    * Mthoed: POST    
    * Body:{
                "app_id": 1, (Mandatory fields)
                "category_id": 1 (Mandatory fields)
            }
    * 
    */
    public function postListByCategoryId(Request $request){

        $body  = $request->all();

        $postList = PostCategories::select('posts.*')
                ->join('posts', 'posts.id', '=', 'post_categories.post_id')
                ->where('posts.application_id', $body['app_id'])
                ->where('post_categories.category_id', $body['category_id'])  
                ->get();

               
        $postIdList = [];
        for($i = 0; $i < count($postList); $i++){
            $postIdList[$i] = $postList[$i]['id'];
        }




        $posts =  Post::where('application_id',$body['app_id']) 
                    ->whereIn('id', $postIdList)
                    ->orderBy('id','DESC')->get();

                    
        $postImages = PostImages::select('post_images.*')
                    ->join('posts', 'posts.id', '=', 'post_images.post_id')
                    ->where('posts.application_id', $body['app_id'])
                    ->whereIn('posts.id', $postIdList)
                    ->get();

        $additionalFields = AdditionalFields::select('additional_fields.*')
                ->join('posts', 'posts.id', '=', 'additional_fields.post_id')
                ->where('posts.application_id', $body['app_id'])
                ->whereIn('posts.id', $postIdList)
                ->get();

        $postCategories = PostCategories::select('categories.*','post_categories.post_id as post_id')
                ->join('posts', 'posts.id', '=', 'post_categories.post_id')
                ->join('categories', 'categories.id', '=', 'post_categories.category_id')
                ->where('posts.application_id', $body['app_id'])
                ->whereIn('posts.id', $postIdList)
                ->get();
               
        $formatedPost = $this->postListFormate($posts, $postImages, $additionalFields, $postCategories);

        if (is_null($posts)) {
            return $this->sendError('Category not found.');
        }

        return $this->sendResponse($formatedPost, 'Post retrieved successfully.');
    }



     /*
    * 
    * localhost:8000/api/post-list-by-search-key
    * Mthoed: POST    
    * Body:{
                "app_id": 1, (Mandatory fields)
                "search_key": 'ds' (Mandatory fields)
            }
    * 
    */
    public function postListBySearchKey(Request $request){

        $body  = $request->all();

        $searchKeyArr = explode(" ", $body['search_key']);
        $searchKey = "";
        for($i=0; $i<count($searchKeyArr); $i++){

            if($searchKeyArr[$i] != ""){
                if($searchKey!= ""){
                    $searchKey .= "%";
                }
                $searchKey .=$searchKeyArr[$i];
            }

        }

        //dd($searchKey);
        $query = " SELECT * FROM posts ".
                    " WHERE application_id = ".$body['app_id'] .
                " AND ( title LIKE '%".$searchKey."%' OR  'desc' LIKE '%".$searchKey."%' ) ";
                //dd($query);

        $postList = DB::select($query);
        $postIdList = [];
        for($i = 0; $i < count($postList); $i++){
            $postIdList[$i] = $postList[$i]->id;
        }

        $posts =  Post::where('application_id',$body['app_id']) 
                ->whereIn('id', $postIdList)
                ->orderBy('id','DESC')->get();

        $postImages = PostImages::select('post_images.*')
                ->join('posts', 'posts.id', '=', 'post_images.post_id')
                ->where('posts.application_id', $body['app_id'])
                ->whereIn('posts.id', $postIdList)
                ->get();

        $additionalFields = AdditionalFields::select('additional_fields.*')
                ->join('posts', 'posts.id', '=', 'additional_fields.post_id')
                ->where('posts.application_id', $body['app_id'])
                ->whereIn('posts.id', $postIdList)
                ->get();

        $postCategories = PostCategories::select('categories.*','post_categories.post_id as post_id')
                ->join('posts', 'posts.id', '=', 'post_categories.post_id')
                ->join('categories', 'categories.id', '=', 'post_categories.category_id')
                ->where('posts.application_id', $body['app_id'])
                ->whereIn('posts.id', $postIdList)
                ->get();
        
        $formatedPost = $this->postListFormate($posts, $postImages, $additionalFields, $postCategories);

       

        if (is_null($formatedPost)) {
            return $this->sendError('Category not found.');
        }


        return $this->sendResponse($formatedPost, 'Post retrieved successfully.');

    }





    private function postListFormate($posts, $postImages, $additionalFields, $postCategories){

        $formatedPost = [];

        for($i = 0; $i < count($posts) ; $i++){

            $formatedPost[$i]["id"] =       $posts[$i]["id"];
            $formatedPost[$i]["title"] =    $posts[$i]["title"];
            $formatedPost[$i]["desc"] =     $posts[$i]["desc"];
            $formatedPost[$i]["phone"] =    is_null($posts[$i]["phone"])?"":$posts[$i]["phone"];
            $formatedPost[$i]["email"] =   is_null($posts[$i]["email"])?"":$posts[$i]["email"];
            $formatedPost[$i]["lati"] =    is_null($posts[$i]["lati"])?"":$posts[$i]["lati"]; 
            $formatedPost[$i]["long"] =     is_null($posts[$i]["long"])?"":$posts[$i]["long"];
            $formatedPost[$i]["is_featured"] =  is_null($posts[$i]["is_featured"])?"":$posts[$i]["is_featured"];
            $formatedPost[$i]["post_type"] =  is_null($posts[$i]["post_type"])?"":$posts[$i]["post_type"];
            $formatedPost[$i]["media_link"] = is_null($posts[$i]["media_link"])?"":$posts[$i]["media_link"]; 
            $formatedPost[$i]["is_public"] =  is_null($posts[$i]["is_public"])?"":$posts[$i]["is_public"];
            $formatedPost[$i]["is_published"] =  is_null($posts[$i]["is_published"])?"":$posts[$i]["is_published"];
            $formatedPost[$i]["alive_date"] =  is_null($posts[$i]["alive_date"])?"":$posts[$i]["alive_date"];
            $formatedPost[$i]["application_id"] = is_null($posts[$i]["application_id"])?"":$posts[$i]["application_id"];
            $formatedPost[$i]["modified_date"] =  isset($posts[$i]["updated_at"])&&!empty($posts[$i]["updated_at"]) ? Carbon::parse($posts[$i]["created_at"])->format('d-m-Y') : Carbon::parse($posts[$i]["updated_at"])->format('d-m-Y') ;

            $formatedPost[$i]["images"] = [];
            $formatedPost[$i]["category_id"] = [];
            $formatedPost[$i]["attachments"] = [];
            $formatedPost[$i]["additionals"] =[];

            for($j = 0; $j < count($postImages) ; $j++){

                if($postImages[$j]['post_id'] == $posts[$i]["id"]){

                    $tempPI = [];
                    $tempPI["id"] =  $postImages[$j]['id'];
                    $tempPI["img_name"] =  $postImages[$j]['img_name'];
                    $tempPI["img_path"] =  $postImages[$j]['img_path'];
                    $tempPI["org_name"] =  $postImages[$j]['org_name'];
                    $tempPI["type"] =  $postImages[$j]['type'];
                    $tempPI["ext"] =  $postImages[$j]['ext'];
                    $tempPI["size"] =  $postImages[$j]['size'];
                    $tempPI["post_id"] =  $postImages[$j]['post_id'];

                    if($postImages[$j]['type'] == 'Attachment'){
                        $count = count( $formatedPost[$i]["attachments"] );
                        $formatedPost[$i]["attachments"][$count] = $tempPI;
                    }
                    else{
                        $count = count( $formatedPost[$i]["images"] );
                        $formatedPost[$i]["images"][$count] = $tempPI;
                    }

                }
                
            }

            for($j = 0; $j < count($additionalFields) ; $j++){

                if($additionalFields[$j]['post_id'] == $posts[$i]["id"]){

                    $tempPI = [];
                    $tempPI["id"] =  $additionalFields[$j]['id'];
                    $tempPI["key"] =  is_null($additionalFields[$j]['key'])?"":$additionalFields[$j]['key'];
                    $tempPI["value"] = is_null($additionalFields[$j]['value'])?"":$additionalFields[$j]['value']; 
                    $tempPI["post_id"] =  $additionalFields[$j]['post_id'];

                    $count = count( $formatedPost[$i]["additionals"] );
                    $formatedPost[$i]["additionals"][$count] = $tempPI;

                }

            }

            for($j = 0; $j < count($postCategories) ; $j++){

                if($postCategories[$j]['post_id'] == $posts[$i]["id"]){

                    $tempPI = [];

                    $tempPI["id"] =  $postCategories[$j]['id'];
                    $tempPI["name"] =  $postCategories[$j]['name'];
                    $tempPI["img_name"] =  $postCategories[$j]['img_name'];
                    $tempPI["image_url"] =  url('uploads/categories/'.$postCategories[$j]['img_name']);
                    $tempPI["post_id"] =  $postCategories[$j]['post_id'];


                    $count = count( $formatedPost[$i]["category_id"] );
                    $formatedPost[$i]["category_id"][$count] = $tempPI;

                }

            }



        }
        return  $formatedPost;
    }









}