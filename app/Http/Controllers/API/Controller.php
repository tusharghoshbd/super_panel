<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{

    public function sendResponse($result, $message)
    {
        if(count($result) == 0){
            $response = [
                'success' => false,
                'data'    => [],
                'message' => "Data doesn't exist",
            ];
        }else{
            $response = [
                'success' => true,
                'data'    => $result,
                'message' => $message,
            ];
        }
    	
        return response()->json($response, 200);
    }



    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
} 