<?php

namespace App\Http\Controllers;

use App\Notification;
use App\NotificationType;
use Illuminate\Http\Request;
use File;
use App\Library\Services\FirebaseService;

class NotificationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::where('application_id',session()->get('application')['id'])->orderBy('id','DESC')->get();
        for($i=0; $i<count($notifications); $i++){
            $notifications[$i]->type_name = NotificationType::find($notifications[$i]->type_id)->name;
        }
        return view('admin.notifications.index')->with('notifications', $notifications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $notificationTypes = NotificationType::where('application_id',session()->get('application')['id'])->orderBy('id','DESC')->get();
        return view('admin.notifications.create')->with('notificationTypes', $notificationTypes);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request, FirebaseService $firebaseService )
    {
        $niceNames = [
            'title' => 'Title',
            'type' => 'Type',
            'body' => 'Body',
            'img_name' => 'Image'
        ];
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'body' => 'required',
            'img_name' => 'image|mimes:jpg,jpeg,png,gif'
        ], [], $niceNames);

     
        //image
        $fileName = null;        
        if ($request->hasFile('img_name')) {
            $file = $request->file('img_name');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./uploads/notifications/', $fileName);
        }

        $notification = new Notification();
        $notification->title = $request->get('title');
        $notification->type_id = $request->get('type');
        $notification->body = $request->get('body') ;
        $notification->img_name = $fileName;
        $notification->application_id =  session()->get('application')['id'];
        $notification->created_by = Auth()->user()->id;

        if ($notification->save()) {

            $notificationType = NotificationType::find($notification->type_id);
            if($fileName)
                $filePath =  url('uploads/notifications/'.$fileName);
            else
                $filePath= "";
            $renResult = $firebaseService->pushNotifications($notification->title, $notificationType->name, $notification->body, $filePath );
            $request->session()->flash('alert-success', 'Notificaion created successfully. '.$renResult);


        } else {
            $request->session()->flash('alert-danger', 'Notificaion creation failed');
        }          

        
        return redirect()->route('notifications.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {        
        $notificationTypes = NotificationType::all();
        return view('admin.notifications.update')->with(['notification'=> $notification, 'notificationTypes'=>$notificationTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        $niceNames = [
            'title' => 'Title',
            'type' => 'Type',
            'body' => 'Body',
            'img_name' => 'Image'
        ];
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'body' => 'required',
            'img_name' => 'image|mimes:jpg,jpeg,png,gif'
        ], [], $niceNames);
        //image
        $fileName = null;        
        
        if ($request->hasFile('img_name')) {
            $file = $request->file('img_name');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./uploads/notifications/', $fileName);

            $image_path = public_path().'/uploads/notifications/'.Notification::find($notification->id)->img_name;
            if(File::exists($image_path))    {
                File::delete($image_path);
            }
        }

        $notification->title = $request->get('title');
        $notification->type_id = $request->get('type');
        $notification->body = $request->get('body') ;
        if($fileName != null)
            $notification->img_name = $fileName;
        $notification->application_id =  session()->get('application')['id'];
        $notification->updated_by = Auth()->user()->id;

        if ($notification->save()) {
            $request->session()->flash('alert-success', 'Notification updated successfully');
        } else {
            $request->session()->flash('alert-danger', 'Notification update failed');
        }       
        return redirect()->route('notifications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        if ($notification->delete()) {
            session()->flash('alert-success', 'Notification deleted successfully');
        } else {
            session()->flash('alert-danger', 'Notification delete failed');
        }
        return redirect()->route('notifications.index');
    }
}
