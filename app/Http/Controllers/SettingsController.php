<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\NotificationType;
use Response;
use App\Configs;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serverKey = Configs::where('application_id',session()->get('application')['id'])
                            ->where('key', 'server-key')
                            ->first();

        //dd($serverKey);
        $notificationTypes = NotificationType::where('application_id',session()->get('application')['id'])->orderBy('id','DESC')->get();
        return view('admin.settings.index')->with(['notificationTypes' => $notificationTypes,'serverKey'=> $serverKey]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function notificationTypeStore(Request $request)
    {
        $niceNames = [
            'name' => 'Type name'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50'
        ],[],$niceNames);

        if($validator->fails()){
            return Response::json(['errors'=>$validator->getMessageBag()->toArray()]);
        }else{
            $notificationType=new NotificationType();
            $notificationType->name=$request->name;
            $notificationType->application_id =  session()->get('application')['id'];
            $notificationType->created_by = Auth()->user()->id;
            $notificationType->save();
            return Response::json('Notification type created successfully.');
        }
    }
    public function notificationTypeUpdate(Request $request)
    {
        $niceNames = [
            'name' => 'Type name'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50'
        ],[],$niceNames);

        if($validator->fails()){
            return Response::json(['errors'=>$validator->getMessageBag()->toArray()]);
        }else{
            $notificationType= NotificationType::find($request->id);
            $notificationType->name=$request->name;
            $notificationType->application_id =  session()->get('application')['id'];
            $notificationType->updated_by = Auth()->user()->id;
            $notificationType->save();
            return Response::json('Notification type updated successfully.');
        }
    }
    public function notificationTypeDelete($id)
    {
        $notificationType = NotificationType::find($id);
        if ($notificationType->delete()) {
            session()->flash('alert-success', 'Notification Type deleted successfully');
        } else {
            session()->flash('alert-danger', 'Notification Type delete failed');
        }
        return redirect()->route('settings.index');
    }


    public function serverKey(Request $request)
    {
        //return Response::json($request->serverKey);
        //dd($request->serverKey);
        $niceNames = [
            'serverKey' => 'Server Key'
        ];
        $validator = Validator::make($request->all(), [
            'serverKey' => 'required'
        ],[],$niceNames);

        
        if($validator->fails()){
            return Response::json(['errors'=>$validator->getMessageBag()->toArray()]);
        }else{
            $serverKey = Configs::where('application_id',session()->get('application')['id'])
                    ->where('key', 'server-key')
                    ->first();

            if($serverKey){

                $serverKey->value =  $request->serverKey;
                $serverKey->application_id = session()->get('application')['id'];
                $serverKey->save();

            }else{
                $configs = new Configs();
                $configs->key = 'server-key';
                $configs->value = $request->serverKey;
                $configs->application_id =  session()->get('application')['id'];
                $configs->save();
            }
            return Response::json('Sever key saved successfully');
        }
    }


    
}
