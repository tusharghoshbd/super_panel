<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Application;
use App\User;
use DB;
use Auth;

class ApplicationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $applications = Application::orderBy('id', 'desc')->get();
        // return view('superAdmin.applications.index')->with('applications', $applications);
        $applications = Application::getApplicationList();
        return view('superAdmin.applications.index')->with('applications', $applications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$users = User::where('name', '!=', 'Super Admin')->orderBy('name', 'ASC')->get();
        $users = User::getAdminList();
        return view('superAdmin.applications.create')->with('users', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $niceNames = [
        //     'name' => 'Application Name',
        //     'user_id' => 'Assign to'
        // ];
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required|max:50',
        //     'user_id' => 'required'
        // ],[],$niceNames);

        // if ($validator->fails()) {
        //     return redirect('applications/create') //change this to your desired url
        //         ->withErrors($validator)
        //         ->withInput();
        // }

        $niceNames = [
            'name' => 'Application Name'
            //'user_id' => 'Assign to'
        ];
        $request->validate([
            'name' => 'required|max:50'
            //'user_id' => 'required'
        ], [], $niceNames);

        $application = new Application();
        $application->name = $request->get('name');
        $application->user_id = 2; //$request->get('user_id');

        if ($application->save()) {
            $request->session()->flash('alert-success', 'Application created successfully');
        } else {
            $request->session()->flash('alert-danger', 'Application creation failed');
        }
        //return redirect()->back()->withInput();
        return redirect()->route('applications.create');
        
        //return view('superAdmin.applications.create');
        //return redirect()->route('applications.create');
        //return redirect("applications/create");
        //return redirect()->action('ApplicationController@index');
        //return  $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $application = Application::findOrFail($id);
        $users = User::getAdminList();
        return view('superAdmin.applications.update')->with(['application' => $application, 'users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd( $request->all());
        $niceNames = [
            'name' => 'Application Name'
            //'user_id' => 'Assign to'
        ];
        $request->validate([
            'name' => 'required|max:50'
            //'user_id' => 'required'
        ], [], $niceNames);

        $application = Application::find($id);
        $application->name = $request->get('name');
        $application->user_id = 2; ///$request->get('user_id');

        if ($application->save()) {
            $request->session()->flash('alert-success', 'Application updated successfully');
        } else {
            $request->session()->flash('alert-danger', 'Application update failed');
        }
        //return redirect()->back()->withInput();
        return redirect()->route('applications.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //dd( $id);
        $application = Application::find($id);
        //dd( $application);
       //$res = Application::where('id',$id)->delete();
        if ($application->delete()) {
            session()->flash('alert-success', 'Application deleted successfully');
        } else {
            session()->flash('alert-danger', 'Application delete failed');
        }
        return redirect()->route('applications.index');
    }

    public function applicationDetail($id)
    {
        $application = Application::find($id);

        session()->put('application', [
            'id' => $application->id,
            'name' => $application->name
        ]);
        return redirect()->action('DashboardController@index');
    }



}