<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Services\CategoryService;
use App\Category;
use DB;
use File;

class CategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CategoryService $categoryService)
    {
        $categories = $categoryService->getStructuredCategories(session()->get('application')['id']);
        $fmtCategoriesForTable = $categoryService->categoriesFormatForTable($categories,'','');
        return view('admin.categories.index')->with('categories', $fmtCategoriesForTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryService $categoryService)
    {
        $categories = $categoryService->getStructuredCategories(session()->get('application')['id']);
        return view('admin.categories.create')->with('categories', $categories);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $niceNames = [
            'title' => 'Title',
            'p_id' => 'Select root',
            'img_name' => 'Add image'
        ];
        $request->validate([
            'title' => 'required|max:50',
            'img_name' => 'required|image|mimes:jpg,jpeg,png,gif'
        ], [], $niceNames);

        $category = Category::Where('name',$request->get('title'))->get();
        if(count($category) == 0 ){
            $fileName = null;
            if ($request->hasFile('img_name')) {
                $file = $request->file('img_name');
                $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('./uploads/categories/', $fileName);
            }
    
            $category = new Category();
            $category->name = $request->get('title');
            $category->img_name = $fileName;
            $category->application_id = session()->get('application')['id'];
            $category->created_by = Auth()->user()->id;
    
            $catData = Category::Where('name', $request->get('p_name'))->first();
            if ($catData)
                $category->p_id = $catData->id;
            else
                $category->p_id = 0;
    
            if ($category->save()) {
                session()->flash('alert-success', 'Category created successfully');
            } else {
                session()->flash('alert-danger', 'Category creation failed');
            }

        }else{
            session()->flash('alert-danger', 'Category Title already exist.');
        }
        return redirect()->route('categories.create');
                
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        dd($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, CategoryService $categoryService)
    {
        //dd($$categoryArr);
        $categories = $categoryService->getStructuredCategories(session()->get('application')['id']);
        $categoryArr = Category::where('id', $category->p_id)->get();
        $category->p_name = count($categoryArr)>0 ? $categoryArr[0]->name : '';

        return view('admin.categories.update')->with(['category' => $category,'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        // print_r($category);
        // dd($request->all());
        $niceNames = [
            'title' => 'Title',
            'p_id' => 'Select root',
            'img_name' => 'Add image'
        ];
        $request->validate([
            'title' => 'required|max:50',
            'img_name' => 'image|mimes:jpg,jpeg,png,gif'
        ], [], $niceNames);

        $fileName = null;
        if ($request->hasFile('img_name')) {
            $file = $request->file('img_name');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./uploads/categories/', $fileName);

            //delete the file folder public folder
            $image_path = public_path().'/uploads/categories/'.Category::find($category->id)->img_name;
            if(File::exists($image_path))    {
                File::delete($image_path);
            }
        }

        $category =  Category::find($category->id);
        $category->name = $request->get('title');
        if($fileName != null)
            $category->img_name = $fileName;

        $category->application_id = session()->get('application')['id'];
        $category->updated_by = Auth()->user()->id;

        $catData = Category::Where('name', $request->get('p_name'))->first();
        if ($catData)
            $category->p_id = $catData->id;
        else
            $category->p_id = 0;

        if ($category->save()) {
            session()->flash('alert-success', 'Category updated successfully');
        } else {
            session()->flash('alert-danger', 'Category update failed');
        }
        return redirect()->route('categories.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //dd($category);
        //$category = Category::find($category->id);
        if ($category->delete()) {
            session()->flash('alert-success', 'Category deleted successfully');
        } else {
            session()->flash('alert-danger', 'Category delete failed');
        }
        return redirect()->route('categories.index');
    }





    
}
