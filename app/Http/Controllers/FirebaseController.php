<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase-admin-sdk.json');
        // $firebase = (new Factory)
        // ->withServiceAccount($serviceAccount)
        // ->withDatabaseUri('https://superpanel-74812.firebaseio.com/')
        // ->create();

        // $database = $firebase->getDatabase();

        // $newPost = $database
        // ->getReference('notifications')
        // ->push([
        // 'title' => 'Laravel FireBase Tutorial' ,
        // 'category' => 'Laravel'
        // ]);
        // echo '<pre>';
        // print_r($newPost->getvalue());



        // $post = array(
        //     'to' => '/topics/wpnewsnotification',
        //     'data' => array (
        //             'title' => 'title',
        //             'message' => 'message',
        //             'newsId' => 'nesid'
        //     )
        //  );

        // $headers = array( 
        //                 'Authorization: key=AIzaSyA915fycU2Qafno8puXtr9GHLRm2-RwKVA',
        //                 'Content-Type: application/json'
        //             );

        // $ch = curl_init();  
        // curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');   
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);    
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
        // $result = curl_exec($ch);
        // if (curl_errno($ch)) {
        // echo 'FCM error: ' . curl_error($ch);
        // } else {
        // echo "<br><div class='textOutput'>Push sent to all devices.</div>";
        // echo "<br><a href='index.php'>Send New Push Notification</a>";
        // }
        // curl_close($ch);








        $url = "https://fcm.googleapis.com/fcm/send";
        $topico = "general";
        $server_key = "AAAA2qh1KDY:APA91bGcGpp28LamrIHs-YCT_yNzeGvs9uGLqIb2RhnOUC0Tj2ujsZH-rIz1arRKx9cMn10nrzwARR-NoPXoN2Mk2UxvlZG9Qk900a1f7E2zyrgZXbseOWl9qDlC7QWmiBRdNPGqLTv_"; //FIREBASE KEY
        $titulo = "Notification Title";
        $corpo = "Notification Message...";
        $legenda = "SubTitle...";
     

            $headers = array
            (
                'Authorization: key='.$server_key,
                'Content-Type: application/json;charset=UTF-8'
            );
         
            $data = array
            (
              'data' =>
              array (
                'title' => $titulo,
                'body' => $corpo,
                'subtitle' => $legenda,
              ),
              'to' => '/topics/'.$topico,
              'priority' => 'high',
              //'restricted_package_name' => 'com.onlyoneapp.test', //IF YOU WANT SEND TO ONLY ONE APP
            );
       

            $content = json_encode($data);
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false );
            $result = curl_exec($curl);
            curl_close($curl);
            $arr = array();
            $arr = json_decode($result,true);
            //dd($arr);
            if ($arr === FALSE) {
                echo "Json invalido!"."<br>";
            } else if (empty($arr)) {
                echo "Json invalido!"."<br>";
            }else{
                if (array_key_exists ('message_id', $arr)){
                    echo "Mensagem enviada! <br>Mensagem id: ".$arr['message_id']."<br>";
                }else{
                    echo "Ocorreu um erro ao enviar a notificação!"."<br>";
                }
            }


    }

}