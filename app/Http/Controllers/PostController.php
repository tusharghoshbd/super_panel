<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Library\Services\CategoryService;
use App\Post;
use App\Category;
use App\PostCategories;
use App\PostImages;
use App\AdditionalFields;
use Auth;
use File;
use Response;
class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('application_id',session()->get('application')['id'])->orderBy('id','DESC')->get();
        for($i=0; $i<count($posts); $i++){

           $categories =  PostCategories::select('name')
                        ->join('categories', 'categories.id', '=', 'post_categories.category_id')
                        ->where('post_id','=',$posts[$i]->id)
                        ->get();
            $string = "";
            for($j=0; $j< count($categories); $j++){
                if($j == 0)
                    $string .= $categories[$j]->name;
                else
                    $string .= ", ".$categories[$j]->name;
            }

            $posts[$i]->category_name = $string;


            $postImages = PostImages::where('post_id', $posts[$i]->id)->first();
            if($postImages)
                $posts[$i]->img_path = $postImages->img_path;
        }
        
        return view('admin.posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryService $categoryService)
    {
        $categories = $categoryService->getStructuredCategories(session()->get('application')['id']);
        return view('admin.posts.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd( $request->file('img_name'));
        $niceNames = [
            'title' => 'Title',
            'desc' => 'Description',
            'category' => 'Category',
            'img_name.*' => 'Image'
        ];
        $request->validate([
            'title' => 'required',
            'desc' => 'required',
            'category' => 'required',
            'img_name.*' => 'image|mimes:jpg,jpeg,png,gif'
        ], [], $niceNames);

        $categoryValidation = true;
        //image
        $fileNameList = [];        
        if ($request->hasFile('img_name')) {
            $file = $request->file('img_name');
            //dd($file);
            for($i=0 ; $i< count($file); $i++){
                $fileNameList[$i] = [];
                $fileNameList[$i]['img_name'] = md5($file[$i]->getClientOriginalName() . time()) . "." . $file[$i]->getClientOriginalExtension();
                $fileNameList[$i]['img_path'] = 'uploads/posts/'.$fileNameList[$i]['img_name'];
                $fileNameList[$i]['org_name'] = $file[$i]->getClientOriginalName();
                $fileNameList[$i]['type'] = 'Image';
                $fileNameList[$i]['ext'] = $file[$i]->getClientOriginalExtension();
                $fileNameList[$i]['size'] = $file[$i]->getClientSize();

                $file[$i]->move('./uploads/posts/', $fileNameList[$i]['img_name'] );
            }
        }

        if ($request->hasFile('attachments')) {

            $file = $request->file('attachments');
            $len = count($fileNameList);
            for($i=0 ; $i< count($file); $i++){
                $fileNameList[$i+$len] = [];
                $fileNameList[$i+$len]['img_name'] = md5($file[$i]->getClientOriginalName() . time()) . "." . $file[$i]->getClientOriginalExtension();
                $fileNameList[$i+$len]['img_path'] = 'uploads/posts/'.$fileNameList[$i+$len]['img_name'];
                $fileNameList[$i+$len]['org_name'] = $file[$i]->getClientOriginalName();
                $fileNameList[$i+$len]['type'] = 'Attachment';
                $fileNameList[$i+$len]['ext'] = $file[$i]->getClientOriginalExtension();
                $fileNameList[$i+$len]['size'] = $file[$i]->getClientSize();

                $file[$i]->move('./uploads/posts/', $fileNameList[$i+$len]['img_name'] );
            }
        }
        

        //dd($fileNameList);
        
        $post = new Post();
        $post->title = $request->get('title');
        $post->desc = $request->get('desc');
        $post->is_featured = $request->has('is_featured') ? 1 : 0;
        $post->post_type = $request->get('post_type');
        $post->media_link = $request->get('media');
        $post->phone = $request->get('phone');
        $post->email = $request->get('email');
        $post->long = $request->get('long');
        $post->lati = $request->get('lati');
        $post->alive_date  = Carbon::parse($request->get('alive_date'))->format('Y-m-d') ;
        $post->is_public = $request->has('is_public') ? 1 : 0;
        $post->is_published = $request->has('is_published') ? 1 : 0;
        $post->application_id =  session()->get('application')['id'];
        $post->created_by = Auth()->user()->id;

       
        $catDataList = explode(",",$request->get('category'));
        //dd($catDataList);
        $catListForInser  = [];
        for($i = 0; $i < count($catDataList); $i++){
           
                $catData = Category::Where('name', trim($catDataList[$i]) )->first();                
                if ($catData){
                    $catListForInser[$i] = [];
                    $catListForInser[$i]['category_id'] = $catData->id;
                }else{
                    $categoryValidation = false;
                }
        }

        $additionFields = explode(",",$request->get('additional_hidden')); 
        $addiFieldsForInser  = [];
        for($i = 0; $i < count($additionFields); $i++){   
            if( $request->get('field_'.$additionFields[$i]) != ""){         
                $addiFieldsForInser[$i]['key'] = $request->get('field_'.$additionFields[$i]);
                $addiFieldsForInser[$i]['value'] = $request->get('value_'.$additionFields[$i]);   
            }        
        }

        
        if ($categoryValidation){

            if ($post->save()) {

                for($i = 0; $i < count($fileNameList); $i++){
                    $fileNameList[$i]['post_id'] = $post->id;
                }
                for($i = 0; $i < count($catListForInser); $i++){
                    $catListForInser[$i]['post_id'] = $post->id;
                }

                for($i = 0; $i < count($addiFieldsForInser); $i++){
                    $addiFieldsForInser[$i]['post_id'] = $post->id;
                }

                PostCategories::insert($catListForInser);
                PostImages::insert($fileNameList);
                AdditionalFields::insert($addiFieldsForInser);

                $request->session()->flash('alert-success', 'Post created successfully');

            } else {
                $request->session()->flash('alert-danger', 'Post creation failed');
            }

        }else{
            $request->session()->flash('alert-danger', 'Category is not selected correctly');
        }
           

        
        return redirect()->route('posts.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, CategoryService $categoryService)
    {
        $post->alive_date = Carbon::parse($post->alive_date)->format('d-m-Y') ;
        $postCategories =  PostCategories::select('name')
                        ->join('categories', 'categories.id', '=', 'post_categories.category_id')
                        ->where('post_id','=',$post->id)
                        ->get();
        $string = "";
        for($j=0; $j< count($postCategories); $j++){
            if($j == 0)
                $string .= $postCategories[$j]->name;
            else
                $string .= ", ".$postCategories[$j]->name;
        }

        $post->category_name = $string;

        $additionFields = AdditionalFields::where('post_id', $post->id)->get();
        $arrayList = "";
        for($i=1; $i<=  count($additionFields)+1 ; $i++){
            if($i != 1 )
                $arrayList .= ",";
            $arrayList .= $i;
        }        
        $categories = $categoryService->getStructuredCategories(session()->get('application')['id']);
        
        $postImages =  PostImages::where('post_id','=',$post->id)->get();

        return view('admin.posts.show')->with([
            'post' => $post, 
            'categories'=> $categories, 
            'additionFields'=>$additionFields, 
            'arrayList'=>$arrayList,
            'postImages' => $postImages
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, CategoryService $categoryService)
    {
        $post->alive_date = Carbon::parse($post->alive_date)->format('d-m-Y') ;
        $postCategories =  PostCategories::select('name')
                        ->join('categories', 'categories.id', '=', 'post_categories.category_id')
                        ->where('post_id','=',$post->id)
                        ->get();
        $string = "";
        for($j=0; $j< count($postCategories); $j++){
            if($j == 0)
                $string .= $postCategories[$j]->name;
            else
                $string .= ", ".$postCategories[$j]->name;
        }

        $post->category_name = $string;

        $additionFields = AdditionalFields::where('post_id', $post->id)->get();
        $arrayList = "";
        for($i=1; $i<=  count($additionFields)+1 ; $i++){
            if($i != 1 )
                $arrayList .= ",";
            $arrayList .= $i;
        }        
        $categories = $categoryService->getStructuredCategories(session()->get('application')['id']);
        
        $postImages =  PostImages::where('post_id','=',$post->id)->get();


        //dd($arrayList);

        
        return view('admin.posts.update')->with([
                    'post' => $post, 
                    'categories'=> $categories, 
                    'additionFields'=>$additionFields, 
                    'arrayList'=>$arrayList,
                    'postImages' => $postImages
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $niceNames = [
            'title' => 'Title',
            'desc' => 'Description',
            'category' => 'Category'
        ];
        $request->validate([
            'title' => 'required',
            'desc' => 'required',
            'category' => 'required'
        ], [], $niceNames);

        $categoryValidation = true;
        //image
        $fileNameList = [];        
        if ($request->hasFile('img_name')) {
            $file = $request->file('img_name');
            //dd($file);
            for($i=0 ; $i< count($file); $i++){
                $fileNameList[$i] = [];
                $fileNameList[$i]['img_name'] = md5($file[$i]->getClientOriginalName() . time()) . "." . $file[$i]->getClientOriginalExtension();
                $fileNameList[$i]['img_path'] = 'uploads/posts/'.$fileNameList[$i]['img_name'];
                $fileNameList[$i]['org_name'] = $file[$i]->getClientOriginalName();
                $fileNameList[$i]['type'] = 'Image';
                $fileNameList[$i]['ext'] = $file[$i]->getClientOriginalExtension();
                $fileNameList[$i]['size'] = $file[$i]->getClientSize();

                $file[$i]->move('./uploads/posts/', $fileNameList[$i]['img_name'] );
            }
        }

        if ($request->hasFile('attachments')) {

            $file = $request->file('attachments');
            $len = count($fileNameList);
            for($i=0 ; $i< count($file); $i++){
                $fileNameList[$i+$len] = [];
                $fileNameList[$i+$len]['img_name'] = md5($file[$i]->getClientOriginalName() . time()) . "." . $file[$i]->getClientOriginalExtension();
                $fileNameList[$i+$len]['img_path'] = 'uploads/posts/'.$fileNameList[$i+$len]['img_name'];
                $fileNameList[$i+$len]['org_name'] = $file[$i]->getClientOriginalName();
                $fileNameList[$i+$len]['type'] = 'Attachment';
                $fileNameList[$i+$len]['ext'] = $file[$i]->getClientOriginalExtension();
                $fileNameList[$i+$len]['size'] = $file[$i]->getClientSize();

                $file[$i]->move('./uploads/posts/', $fileNameList[$i+$len]['img_name'] );
            }
        }
        //image
        // $fileName = null;        
        
        // if ($request->hasFile('img_name')) {
        //     $file = $request->file('img_name');
        //     $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file->move('./uploads/posts/', $fileName);

        //     $image_path = public_path().'/uploads/posts/'.Post::find($post->id)->img_name;
        //     if(File::exists($image_path))    {
        //         File::delete($image_path);
        //     }
        // }

        $post->title = $request->get('title');
        $post->desc = $request->get('desc');
        $post->is_featured = $request->has('is_featured') ? 1 : 0;
        $post->post_type = $request->get('post_type');
        $post->media_link = $request->get('media');
        $post->phone = $request->get('phone');
        $post->email = $request->get('email');
        $post->long = $request->get('long');
        $post->lati = $request->get('lati');
        $post->alive_date  = Carbon::parse($request->get('alive_date'))->format('Y-m-d') ;
        $post->is_public = $request->has('is_public') ? 1 : 0;
        $post->is_published = $request->has('is_published') ? 1 : 0;
        $post->application_id =  session()->get('application')['id'];
        $post->updated_by = Auth()->user()->id;

        //$catData = Category::Where('name', $request->get('category'))->first();
        $catDataList = explode(",",$request->get('category'));
        //dd($catDataList);
        $catListForInser  = [];
        for($i = 0; $i < count($catDataList); $i++){
           
                $catData = Category::Where('name', trim($catDataList[$i]) )->first();                
                if ($catData){
                    $catListForInser[$i] = [];
                    $catListForInser[$i]['category_id'] = $catData->id;
                }else{
                    $categoryValidation = false;
                }
        }

        $additionFields = explode(",",$request->get('additional_hidden')); 
        $addiFieldsForInser  = [];
        for($i = 0; $i < count($additionFields); $i++){   
            $field =  trim($request->get('field_'.$additionFields[$i]) );
            if( $field != ""){
                $addiFieldsForInser[$i]['key'] = $field;
                $addiFieldsForInser[$i]['value'] = trim( $request->get('value_'.$additionFields[$i]) );  
            }                     
        }


        if ($categoryValidation){
            //$post->category_id = $catData->id;

            //insert
            for($i = 0; $i < count($fileNameList); $i++){
                $fileNameList[$i]['post_id'] = $post->id;
            }

            //delete insert
            for($i = 0; $i < count($catListForInser); $i++){
                $catListForInser[$i]['post_id'] = $post->id;
            }
            //delete insert
            for($i = 0; $i < count($addiFieldsForInser); $i++){
                $addiFieldsForInser[$i]['post_id'] = $post->id;
            }           

            if ($post->save()) {

                $collection = PostCategories::where('post_id', $post->id)->get(['id']);
                PostCategories::destroy($collection->toArray());

                $collection = AdditionalFields::where('post_id', $post->id)->get(['id']);
                AdditionalFields::destroy($collection->toArray());

                PostCategories::insert($catListForInser);
                PostImages::insert($fileNameList);
                AdditionalFields::insert($addiFieldsForInser);
                $request->session()->flash('alert-success', 'Post updated successfully');
            } else {
                $request->session()->flash('alert-danger', 'Post update failed');
            }

        }else{
            $request->session()->flash('alert-danger', 'Category is not selected correctly');
        }

       
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $collection = PostCategories::where('post_id', $post->id)->get(['id']);
        PostCategories::destroy($collection->toArray());

        $collection = AdditionalFields::where('post_id', $post->id)->get(['id']);
        AdditionalFields::destroy($collection->toArray());

        $collection = PostImages::where('post_id', $post->id)->get();
        for($i = 0; $i < count($collection); $i++){
            $image_path = public_path().'/'.$collection[$i]->img_path;
            if(File::exists($image_path))    {
                File::delete($image_path);
            }
        }
        
        $collection = PostImages::where('post_id', $post->id)->get(['id']);
        PostImages::destroy($collection->toArray());
        

        if ($post->delete()) {
            session()->flash('alert-success', 'Post deleted successfully');
        } else {
            session()->flash('alert-danger', 'Post delete failed');
        }
        return redirect()->route('posts.index');
    }


    public function imageFileDelete(Request $request){


        $postImges = PostImages::find($request->get('id'));

        $image_path = public_path().'/'.$postImges->img_path;
        if(File::exists($image_path))    {
            File::delete($image_path);
        }

        if ($postImges->delete()) {
            return Response::json('File deleted successfully.');
        } else {
            return Response::json('File delete failed.');
        }

        
    }
}
