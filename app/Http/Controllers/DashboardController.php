<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use Auth;
use App\Post;
use App\Category;
use App\PostCategories;
use App\PostImages;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        

        $postCount = 0;
        $categoryCount =0;
        if(session()->get('application')){
            $posts = Post::where('application_id',session()->get('application')['id'])->get();
            $postCount = $posts->count();
            $categoryCount = Category::where('application_id',session()->get('application')['id'])->get()->count();
        }
        

        if (Auth::user()->role == 'super_admin') {
            $applications = Application::getApplicationList();
            return view('dashboard')
                ->with([
                    'applications'=> $applications,
                    'postCount'=> $postCount, 
                    'categoryCount' => $categoryCount
                ]);
        } 
        else {           
            return view('dashboard')->with(['postCount'=> $postCount, 'categoryCount' => $categoryCount ]);
        }
    }


    public function redirectToSaPanel(){

        session()->forget('application');
        return redirect()->route('dashboard');
    }
}
