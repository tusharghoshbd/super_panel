<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB; 

class Application extends Model
{
    //

    public $timestamps = true;

   public function user()
   {
      return $this->belongsTo('App\User', 'user_id');
   }
   public static function getApplicationList()
   {
      return DB::table('applications')
         // ->join('users', 'users.id', '=', 'applications.user_id')
         ->select("applications.*", "applications.name as app_name")
         ->orderBy('applications.id', 'desc')
         ->get();
   }
}
