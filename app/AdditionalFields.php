<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalFields extends Model
{
    //
    public $timestamps = true;
}
