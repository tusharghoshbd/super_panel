<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\FirebaseService;
class FirebaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\FirebaseService', function ($app) {
            return new FirebaseService();
        });
    }
}
