<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\CategoryService;
class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\CategoryService', function ($app) {
            return new CategoryService();
        });
    }
}
