<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'prevent-back-history'],function(){

    Auth::routes();
    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/redirect_to_sa_panel', 'DashboardController@redirectToSaPanel');

    // Route::get('/applications', function () {
    //     return view('pages.applications.index');
    // });

    // Route::get('/applications/create', function () {
    //     return view('pages.applications.create');
    // });

    Route::resource('applications', 'ApplicationController');
    Route::get('/application_detail/{id}', 'ApplicationController@applicationDetail');

    Route::resource('categories', 'CategoryController');
    Route::resource('posts', 'PostController');
    Route::post('/image-file-delete', 'PostController@imageFileDelete')->name('imageFileDelete');

    Route::resource('notifications', 'NotificationController');

    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/notification-type', 'SettingsController@notificationTypeStore')->name('notificationTypeCreate');
    Route::put('/notification-type', 'SettingsController@notificationTypeUpdate')->name('notificationTypeUpdate');
    Route::delete('/notification-type/{id}', 'SettingsController@notificationTypeDelete')->name('notificationTypeDelete');

    Route::post('/server-key', 'SettingsController@serverKey')->name('serverKey');

    Route::get('/firebase','FirebaseController@index');
    // Route::resources([
    //     'applications' => 'ApplicationController'
    // ]);
});
  

