<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/category-list', 'API\CategoryController@categoryList');
Route::post('/post-list', 'API\PostController@postList');
Route::post('/post-list-by-category-id', 'API\PostController@postListByCategoryId');
Route::post('/post-list-by-search-key', 'API\PostController@postListBySearchKey');

//  Route::get('/category', function () {
//      return "test";
//     //return response()->json("test", 200);
// });
  
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
