<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try { ace.settings.loadState('sidebar') } catch (e) { }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="custom-menu-item nav nav-list">
            <li  id="dashboardMenuId" class="">
                <a href="{{ URL::to('/') }}">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>
    
                <b class="arrow"></b>
            </li>
        @if (Auth::user()->role == 'super_admin' && !session()->has('application'))
            <li id="applciationsMenuId" class="">
                {{--   class="dropdown-toggle"  --}}
                <a href="{{ URL::to('/applications') }}">
                    <i class="menu-icon fa fa-desktop"></i>
                    <span class="menu-text">
                        Applications
                    </span>
    
                    {{--  <b class="arrow fa fa-angle-down"></b>  --}}
                </a>
    
                <b class="arrow"></b>
    
                {{--  <ul class="submenu">
                    <li class="">
                        <a href="{{ URL::to('/applications') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            List view
                        </a>
    
                        <b class="arrow"></b>
                    </li>
    
                    <li class="">
                        <a href="{{ URL::to('/applications/create') }}">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Creat new
                        </a>
    
                        <b class="arrow"></b>
                    </li>
    
                    
                </ul>  --}}
            </li> 
        @else
            <li id="postsMenuId" class="">
                <a href="{{ URL::to('/posts') }}">
                    <i class="menu-icon fa fa-plus-square"></i>
                    <span class="menu-text">
                        All Post
                    </span>

                </a>
            </li>
            <li id="categoriesMenuId" class="">
                <a href="{{ URL::to('/categories') }}">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text">
                        Categories
                    </span>

                </a>
            </li>
            <li id="notificationsMenuId" class="">
                <a href="{{ URL::to('/notifications') }}">
                    <i class="menu-icon  fa fa-bell "></i>
                    <span class="menu-text">
                        Notifications
                    </span>

                </a>
            </li>
            <li id="settingsMenuId" class="">
                <a href="{{ URL::to('/settings') }}">
                    <i class="menu-icon  fa fa-cog "></i>
                    <span class="menu-text">
                        Settings
                    </span>

                </a>
            </li>
            @if (Auth::user()->role == 'super_admin')
                <li id="redirectMenuId">
                    <a href="{{ URL::to('/redirect_to_sa_panel') }}">
                        <i class="menu-icon fa fa-arrow-left"></i>
                        <span class="menu-text"> Back to SA Panel </span>
                    </a>
                </li> 
            @endif
        @endif
        


       
        
    </ul>
</div>