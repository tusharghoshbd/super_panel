@extends('layouts.default') 
@section('css')

<link rel="stylesheet" href="{{ URL::asset('css/slick.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/slick-style.css') }}" />
@endsection
 
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>
            <li class="active">Dashboard</li>
        </ul>
        <!-- /.breadcrumb -->
    </div>

    <div class="page-content">
        @if ( Auth::user()->role == 'super_admin' && !session()->has('application') )
            <div class="row">

                @foreach($applications as $key => $application)
                    <div class="shortcuts">
                        <a href="{{ URL::to('/application_detail/'.$application->id) }}"  class="shortcut" style="background-color:#1abc9c;">
                            <i class="shortcut-icon fa fa-list-alt"></i>
                        </a>
                        <span class="shortcut-label">
                            <a href="{{ URL::to('/application_detail/'.$application->id) }}"  >
                                {{$application->name}}
                            </a>
                        </span>
                    </div>
                @endforeach               
    
            </div>
        @else



            <div class="row">
                <div class="space-6"></div>

                <div class="col-sm-12 infobox-container">
                    <div class="infobox infobox-green">
                        <div class="infobox-icon">
                            <i class="ace-icon fa fa-comments"></i>
                        </div>

                        <div class="infobox-data" style="text-align:right">
                            <span class="infobox-data-number" style="font-size:35px"  >{{$categoryCount}}</span>
                            <div class="infobox-content">Number Of Categories</div>
                        </div>
                    </div>

                    <div class="infobox infobox-blue">
                        <div class="infobox-icon">
                            <i class="ace-icon fa fa-twitter"></i>
                        </div>

                        <div class="infobox-data" style="text-align:right">
                            <span class="infobox-data-number" style="font-size:35px"  >{{$postCount}}</span>
                            <div class="infobox-content">Number of Posts</div>
                        </div>
                    </div>


                </div>

                <div class="vspace-12-sm"></div>

            </div>  
            <div class="hr hr32 hr-dotted"></div>

            <div class="row">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        @endif

       
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    $( document ).ready(function() {

            
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #dashboardMenuId").addClass("active");
        
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Post and comments Statistics'
            },
            subtitle: {
                text: 'Source: WorldClimate.com'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rainfall (mm)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Tokyo',
                data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        
            }, {
                name: 'New York',
                data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
        
            }, {
                name: 'London',
                data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
        
            }, {
                name: 'Berlin',
                data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
        
            }]
        });

	});
</script>
@endsection