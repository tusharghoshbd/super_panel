<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<meta name="_token" content="{{ csrf_token() }}">
	<title>Super Panel - Super Admin</title>

	<meta name="description" content="Static &amp; Dynamic Tables" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	
	
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('font-awesome/4.5.0/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/fonts.googleapis.com.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.min.css') }}" />
	
	<!-- ace styles -->
	<link rel="stylesheet" href="{{ URL::asset('css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
	<link rel="stylesheet" href="{{ URL::asset('css/ace-skins.min.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/ace-rtl.min.css') }}" />
	@yield('css')

	<link rel="stylesheet" href="{{ URL::asset('css/custom-style.css') }}" />


	<script src="{{ URL::asset('js/ace-extra.min.js') }}"></script>


	<script src="{{ URL::asset('js/jquery-2.1.4.min.js') }}"></script>
	<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/ace-elements.min.js') }}"></script>
	<script src="{{ URL::asset('js/ace.min.js') }}"></script>

	@yield('js')

	

</head>

<body class="no-skin">
	
	@include('includes.header')

	<div class="main-container ace-save-state" id="main-container">
	
		@include('includes.sidebar')
		
		<div class="main-content">
			@yield('content')
			
		</div>

		@include('includes.footer')

	</div>

</body>

</html>