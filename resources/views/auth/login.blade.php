<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Super Panel - Super Admin</title>

		<meta name="description" content="Static &amp; Dynamic Tables" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('font-awesome/4.5.0/css/font-awesome.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/fonts.googleapis.com.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ URL::asset('css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="{{ URL::asset('css/ace-skins.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/ace-rtl.min.css') }}" />

		<link rel="stylesheet" href="{{ URL::asset('css/custom-style.css') }}" />


		<script src="{{ URL::asset('js/ace-extra.min.js') }}"></script>

	</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-leaf green"></i>
									<span class="red">Super Panel</span>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6">
											
												<div class="flash-message">
													@foreach (['danger', 'warning', 'success', 'info'] as $msg)
														@if(Session::has('alert-' . $msg))
															<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
														@endif
													@endforeach
												</div>
											</div>

											<form  role="form"  method="post" action="{{ route('login') }}">
													<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
												<fieldset>
													<label class="{{ $errors->has('email') ? 'has-error' : '' }}  block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="email" class="form-control" placeholder="Email" />
															<i class="ace-icon fa fa-user"></i>
															@if ($errors->has('email'))
																<div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('email') }} </div>
															@endif
															
														</span>
													</label>

													<label class="{{ $errors->has('password') ? 'has-error' : '' }} block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Password" name="password" />
															<i class="ace-icon fa fa-lock"></i>

															@if ($errors->has('password'))
																<div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('password') }} </div>
															@endif
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">

														<button class=" button width-35 pull-right btn btn-sm btn-primary" type="submit">
															<i class="ace-icon fa fa-key"></i>
															Login
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

										
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="#" data-target="#forgot-box" class="forgot-password-link">
												</a>
											</div>

											<div>
												<a href="#" data-target="#signup-box" class="user-signup-link">
												</a>
											</div>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->
							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="{{ URL::asset('js/jquery-2.1.4.min.js') }}"></script>
	</body>
</html>
