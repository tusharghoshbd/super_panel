@extends('layouts.default') 
@section('css')

@endsection
 
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>
{{--  
            <li>
                <a href="#"></a>
            </li>  --}}
            <li class="active">Settings</li>
        </ul>
        <!-- /.breadcrumb -->
    </div>

    <div class="page-content">


        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="header smaller lighter blue">Settings</h3>


                        {{--
                        <div class="clearfix">
                            <div class="pull-right tableTools-container"></div>
                        </div> --}}
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">
                                        {{ Session::get('alert-' . $msg) }}
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </p>
                                @endif 
                            @endforeach
                        </div>

                        <div class="table-header">
                            Notificaton Type List {{--
                            <div class="row">
                                <div class="col-6">
                                    Results for "Latest Registered Domains"
                                </div>
                                <div class="col-6">

                                </div>
                            </div> --}}
                            <a href="#" id="notification-type-create-id" class="btn btn-success pull-right">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Create
                            </a>

                        </div>
                        <div>
                            <table id="dynamic-table" class=" table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Name </th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($notificationTypes as $notificationType)
                                        <tr>
                                            <td>{{$notificationType->name}}</td>
                                            <td>{{ \Carbon\Carbon::parse($notificationType->created_at)->format('d-m-Y')}}</td>
                                            <td>
                                                <div> 
                                                    <a class="green" href="#" onclick="editNotificationUpdate({{$notificationType}})">
                                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                    </a>                                    

                                                    <a class="red" href="javascript:void(0)"  onclick="event.preventDefault();
                                                            if(confirm('Are you sure?')){document.getElementById('delete-form_{{$notificationType->id}}').submit();}">
                                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                    </a>
                                                    <form id="delete-form_{{$notificationType->id}}" action="/notification-type/{{$notificationType->id}}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div id="dialog-form-create" title="Create New Notification Type">
                                <form class="create">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                    <div class="form-group ">

                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type name <span class="red">*</span></label>
                
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" id="form-field-1" value="" name="name" placeholder="Type name"  autocomplete='off' />
                                            <span class="text-danger typenameError"></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9 no-padding-left">
                                            <button class="btn btn-primary" type="button" id="notification-type-form-create-id">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Create
                                            </button>
                                            <a href="javascript::void(0)" id="notification-type-exit-id" class="btn btn-danger" >
                                                <i class="ace-icon fa fa-close bigger-110"></i>
                                                Exit
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="dialog-form-update"  title="Update Notification Type">
                                <form class="update">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                    <div class="form-group ">

                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type name <span class="red">*</span></label>
                
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" id="form-field-1" value="" name="edit_name" placeholder="Type name"  autocomplete='off' />
                                            <input type="hidden" id="hid_edit_name_id" name="hid_edit_name_id" value=""  />
                                            <span class="text-danger typenameError"></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9 no-padding-left">
                                            <button class="btn btn-primary" type="button" id="notification-type-form-update-id">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Update
                                            </button>
                                            <a href="javascript::void(0)" id="notification-type-exit-id" class="btn btn-danger" >
                                                <i class="ace-icon fa fa-close bigger-110"></i>
                                                Exit
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <h3 class="header smaller lighter blue"> Firebase Server Key </h3>
                        <div class="row">
                            <div class="col col-md-2">
                                <label >Server Key <span class="red">*</span></label> 
                            </div>
                            <div class="col col-md-8">
                                <div class="form-group ">
                                    <input type="text" name="server-key"  value="{{ is_null($serverKey) ? '' : $serverKey->value }}" class="form-control" id="server-key-inp-id" />
                                    <span class="text-danger serverkeyeError"></span>
                                </div>
                            </div>
                            <div class="col col-md-2">
                                <button class="btn btn-primary" id="server-key-btn-id">
                                    <i class="ace-icon fa fa-check bigger-100"></i>
                                    Save
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js') 

<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.bootstrap.min.js') }}"></script>
{{-- <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script> --}}


<script type="text/javascript">



    {{-- var config = {
        apiKey: "AIzaSyA915fycU2Qafno8puXtr9GHLRm2-RwKVA",
        authDomain: "superpanel-74812.firebaseapp.com",
        databaseURL: "https://superpanel-74812.firebaseio.com"
    };
    firebase.initializeApp(config);
    
    var database = firebase.database();   

    var dialog_create,dialog_update; --}}



    $( document ).ready(function() {
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #settingsMenuId").addClass("active");
        
        dialog_create = $("#dialog-form-create" ).dialog({
            autoOpen: false,
            height: 200,
            width: 400,
            modal: true
        });   
        dialog_update = $("#dialog-form-update" ).dialog({
            autoOpen: false,
            height: 200,
            width: 400,
            modal: true
        });      

        var table = $('#dynamic-table').DataTable({
            "ordering": false
        });

        $("#notification-type-create-id").click(function(){
            dialog_create.dialog( "open" );
        });
        $(".create #notification-type-exit-id").click(function(){
            dialog_create.dialog( "close" );
        });
        $(".update #notification-type-exit-id").click(function(){
            dialog_update.dialog( "close" );
        });


    
        
        $("#notification-type-form-update-id").click(function(event){
            event.preventDefault();
            
            $.ajax({
                type:'PUT',
                url:'/notification-type',
                headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
                data:{
                    name: $('input[name=edit_name]').val(),
                    id : $('input[name=hid_edit_name_id]').val()
                },
                success:function(data){
                    if((data.errors)){
                        if(data.errors.name){
                            $('.typenameError').text(data.errors.name);
                        }else{
                            $('.typenameError').text(''); 
                        }

                    }else{
                        $('input[name=edit_name]').val('')
                        alert(data);
                        location.reload();
                    }
                }
            });
        })


        $("#notification-type-form-create-id").click(function(event){
            event.preventDefault();
            var name = $('input[name=name]').val();
            
            $.ajax({
                type:'POST',
                url:'/notification-type',
                headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
                data:{
                    name:name
                },
                success:function(data){
                    if((data.errors)){
                        if(data.errors.name){
                            $('.typenameError').text(data.errors.name);
                        }else{
                            $('.typenameError').text(''); 
                        }
    
                    }else{
                        $('input[name=name]').val('')
                        alert(data);
                        location.reload();
                    }
                }
            });

        })





        
        $("#server-key-btn-id").click(function(event){
            event.preventDefault();
            var serverKey = $('input[name=server-key]').val();
            
            $.ajax({
                type:'POST',
                url:'/server-key',
                headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
                data:{
                    serverKey:serverKey
                },
                success:function(data){
                    if((data.errors)){
                        if(data.errors.serverKey){
                            $('.serverkeyeError').text(data.errors.serverKey);
                        }else{
                            $('.serverkeyeError').text(''); 
                        }
    
                    }else{
                        alert(data);
                        //location.reload();
                    }
                }
            });

        })



        {{-- $("#firebase-setting-id").click(function(){
            
            firebase.database().ref('notifications/2').set({
                first_name: "test 2",
                last_name: "test",
            });
            console.log("done");
        }); --}}
        //firebase



       
        

     });// end of document
     
     function editNotificationUpdate(data){
        dialog_update.dialog( "open" );
        $('input[name=edit_name]').val(data.name);
        $('input[name=hid_edit_name_id]').val(data.id);
    }

     

</script>
@endsection