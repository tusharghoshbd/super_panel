@extends('layouts.default') 
@section('css')
<link rel="stylesheet" href="{{ URL::asset('css/jquery.treetable.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/jquery.treetable.theme.default.css') }}" />
@endsection
 
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">Category</a>
            </li>
            <li class="active">List view</li>
        </ul>
        <!-- /.breadcrumb -->
    </div>

    <div class="page-content">


        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="header smaller lighter blue">Categories</h3>


                        {{--
                        <div class="clearfix">
                            <div class="pull-right tableTools-container"></div>
                        </div> --}}
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">
                                        {{ Session::get('alert-' . $msg) }}
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </p>
                                @endif 
                            @endforeach
                        </div>

                        <div class="table-header">
                            Category List {{--
                            <div class="row">
                                <div class="col-6">
                                    Results for "Latest Registered Domains"
                                </div>
                                <div class="col-6">

                                </div>
                            </div> --}}
                            <a href="{{ URL::to('/categories/create') }}" class="btn btn-success pull-right">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Create
                            </a>

                        </div>

                        <table id="example-advanced">
                            <thead>
                                <tr>
                                    <th><b>Name</b></th>
                                    <th><b>Image</b></th>
                                    <th><b>Create at</b></th>
                                    <th><b>Action</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr data-tt-id='{{ $category['child_id'] }}' 
                                        @if($category['parent_id'] !='')
                                            data-tt-parent-id='{{ $category['parent_id'] }}'
                                         @endif
                                         >
                                        <td><span class='file'>{{ $category['name'] }}</span></td>
                                        <td>
                                            <img alt="" height="20px" width="20px" src="uploads/categories/{{$category['img_name'] }}"/>

                                        </td>
                                        <td>{{$category['created_at']}}</td>
                                        <td> 
                                            <a class="green" href="{{URL::to('/categories/'.$category['id'].'/edit') }}">
                                                <i class="ace-icon fa fa-pencil bigger-130"></i>
                                            </a>  
                                            &nbsp;&nbsp;
                                            <a class="red" href="javascript:void(0)"  onclick="event.preventDefault();
                                                        if(confirm('Are you sure?')){document.getElementById('delete-form_{{$category['id']}}').submit();}">
                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                            </a>
                                            <form id="delete-form_{{$category['id']}}" action="{{ route('categories.destroy', $category['id']) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js') {{--
<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.bootstrap.min.js') }}"></script> --}}
<script src="{{ URL::asset('js/jquery.treetable.js') }}"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #categoriesMenuId").addClass("active");



        $("#example-advanced").treetable({ expandable: true });

        // Highlight selected row
        $("#example-advanced tbody").on("mousedown", "tr", function() {
            $(".selected").not(this).removeClass("selected");
            $(this).toggleClass("selected");
        });
     });

</script>
@endsection