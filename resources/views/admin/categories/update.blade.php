@extends('layouts.default') 
@section('css')
<link rel="stylesheet" href="{{ URL::asset('css/comboTreePlugin.css') }}" />
<style>
    .comboTreeDropDownContainer{
        z-index: 100;
    }
    .comboTreeArrowBtn{
        pointer-events: none !important;
        cursor: default !important;
    }

</style>
@endsection
    
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">Category</a>
            </li>
            <li class="active">Update</li>
        </ul>
        <!-- /.breadcrumb -->

    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Update category
                <!-- <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Common form elements and layouts
                </small> -->
            </h1>
        </div>
        <!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                
                {{--  @if (count($errors) > 0)
                    <div class = "alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif  --}}
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                      @if(Session::has('alert-' . $msg))
                
                      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                      @endif
                    @endforeach
                </div>

                <form  enctype="multipart/form-data" class="form-horizontal" role="form"  method="post" action="/categories/{{$category->id}}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group ">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title <span class="red">*</span></label>

                        <div class="col-sm-6 {{ $errors->has('title') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" id="form-field-1" value="{{ $category->name }}" name="title" placeholder="Title"  />
                            @if ($errors->has('title'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('title') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Select root </label>

                        <div class="col-sm-6 {{ $errors->has('p_name') ? 'has-error' : '' }} ">
                            <input type="text" name="p_name" value="{{$category->p_name}}" class="form-control" id="categoryid" placeholder="Select an option" autocomplete='off'/>
                            @if ($errors->has('p_name'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('p_name') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Add image </label>
                        <div class="col-sm-6 {{ $errors->has('img_name') ? 'has-error' : '' }}">
                            <input type="file" name="img_name" class="" id="id-input-file-2"  />
                            @if ($errors->has('img_name'))
                                <div class="help-block col-xs-12 col-sm-reset inline "> {{ $errors->first('img_name') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                        
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9 no-padding-left">
                            <button class="btn btn-primary" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Update
                            </button>
                            <a href="{{ URL::to('/categories') }}" class="btn btn-danger" >
                                <i class="ace-icon fa fa-close bigger-110"></i>
                                Exit
                            </a>
                        </div>
                    </div>



                </form>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js')
<script src="{{ URL::asset('js/comboTreePlugin.js') }}"></script>
<script type="text/javascript">

    var catData =  {!! json_encode($categories) !!};

    $( document ).ready(function() {
        
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #categoriesMenuId").addClass("active");

        $('#id-input-file-2').ace_file_input({
            no_file:'No File ...',
            btn_choose:'Choose',
            btn_change:'Change',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });


        $('#categoryid').comboTree({
			source : catData,
			isMultiple: false
		});
    });
</script>
@endsection