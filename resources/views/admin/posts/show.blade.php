@extends('layouts.default') 
@section('css') {{--
<link rel="stylesheet" href="{{ URL::asset('css/summernote.css') }}" /> --}}
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('css/comboTreePlugin.css') }}" />
<style>
    .comboTreeDropDownContainer {
        z-index: 100;
    }

    .comboTreeArrowBtn {
        pointer-events: none !important;
        cursor: default !important;
    }
</style>
@endsection
 
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">Post</a>
            </li>
            <li class="active">View</li>
        </ul>
        <!-- /.breadcrumb -->

    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Post Details
                <!-- <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Common form elements and layouts
                </small> -->
            </h1>
        </div>
        <!-- /.page-header -->

        <div class="row">
            <div class="col-xs-10">
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg) @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif @endforeach
                </div>
                <form enctype="multipart/form-data" class="form-horizontal" role="form" method="post" action="/posts/{{$post->id}}">

                    @method('PATCH') @csrf
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Title </label>
                        <div class="col-sm-10 {{ $errors->has('title') ? 'has-error' : '' }}">
                            <input type="text" id="form-field-1" value="{{ $post->title }}" name="title" placeholder="Title" class="form-control" />                            @if ($errors->has('title'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('title') }} </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Description </label>
                        <div class="col-sm-10  summernote {{ $errors->has('desc') ? 'has-error' : '' }}">
                            <textarea id="desc-id" name="desc">{{ $post->desc }}</textarea> @if ($errors->has('desc'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('desc') }} </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Category </label>

                        <div class="col-sm-4 {{ $errors->has('category') ? 'has-error' : '' }} ">
                            <input type="text"  name="category" value="{{$post->category_name}}" class="form-control" id="categoryid" placeholder="Select an option"
                                autocomplete='off' /> @if ($errors->has('category'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('category') }} </div>
                            @endif
                        </div>
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Post Type </label>

                        <div class="select col-sm-4 {{ $errors->has('post_type') ? 'has-error' : '' }} ">

                            <select class="form-control" disabled name='post_type'>
                                <option @if ($post->post_type == 'General') {{'selected'}} @endif value="General">General</option>
                                <option @if ($post->post_type == 'Image') {{'selected'}} @endif value="Image">Image</option>
                                <option @if ($post->post_type == 'Media') {{'selected'}} @endif value="Media">Media</option>
                                <option @if ($post->post_type == 'File') {{'selected'}} @endif value="File">File</option>
                            </select> @if ($errors->has('post_type'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('post_type') }} </div>
                            @endif
                        </div>
                        {{--
                        <div class="col-sm-2 "></div>
                        <div class="col-sm-4 post ">
                            <div class="checkbox">
                                <label>
                                    @if ($post->is_featured == 1 )
                                         <input  value="1" checked name='is_featured' type="checkbox" class="ace" />
                                    @else
                                        <input  value="1" name='is_featured' type="checkbox" class="ace" />
                                    @endif
                                    
                                    <span class="lbl"> Featured</span>
                                </label>
                            </div>
                        </div> --}}
                    </div>

                    {{--
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Image </label>
                        <div class="col-sm-10 {{ $errors->has('img_name') ? 'has-error' : '' }}">

                            <input type="file" name="img_name" class="" id="id-input-file-2" /> @if ($errors->has('img_name'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('img_name') }} </div>
                            @endif
                        </div>
                    </div> --}}

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Images </label>
                        
                        <div class="col-sm-10 ">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Image</td>
                                    <td>Name</td>
                                </tr>
                                @for ($i = 0; $i
                                < count($postImages); $i++) @if ($postImages[$i][ 'type']=='Image' ) 
                                <tr id="imageRowId_{{$postImages[$i]['id']}}">
                                    <td><img alt="" height="20px" width="20px" src="/{{$postImages[$i]['img_path']}}" /></td>
                                    <td>{{ $postImages[$i]['org_name'] }}</td>
                                </tr>
                                @endif @endfor

                            </table>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Media </label>
                        <div class="col-sm-10 {{ $errors->has('media') ? 'has-error' : '' }}">
                            <input type="text" id="form-field-1" value="{{ $post->media_link }}" name="media" placeholder="Media" class="form-control"
                            /> @if ($errors->has('media'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('media') }} </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Phone </label>

                        <div class="col-sm-4 {{ $errors->has('phone') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ $post->phone }}" name="phone" placeholder="Phone" class="form-control" />                            @if ($errors->has('phone'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('phone') }} </div>
                            @endif
                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Email </label>

                        <div class="col-sm-4 {{ $errors->has('email') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ $post->email }}" name="email" placeholder="Email" class="form-control" />                            @if ($errors->has('email'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('email') }} </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Latitude </label>

                        <div class="col-sm-4 {{ $errors->has('lati') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ $post->lati }}" name="lati" placeholder="Latitude" class="form-control" />                            @if ($errors->has('lati'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('lati') }} </div>
                            @endif
                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Longitude </label>

                        <div class="col-sm-4 {{ $errors->has('long') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ $post->long }}" name="long" placeholder="Longitude" class="form-control" />                            @if ($errors->has('long'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('long') }} </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="alive-date-id"> Alive Date </label>

                        <div class="col-sm-4 {{ $errors->has('alive_date') ? 'has-error' : '' }} ">

                            <div class="input-group">
                                <input class="form-control date-picker" value="{{ $post->alive_date }}" name="alive_date" id="alive-date-id" type="text"
                                    data-date-format="dd-mm-yyyy" />
                                <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                            </div>
                            @if ($errors->has('alive_date'))
                            <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('alive_date') }} </div>
                            @endif
                        </div>

                        <label class="col-sm-1 control-label no-padding-right" for="form-field-1-1">  </label>



                        <div class="col-sm-5 post ">
                            <div class="checkbox">
                                <label>
                                    @if ($post->is_public == 1 )
                                        <input  value="1" checked name='is_public' type="checkbox" class="ace" />
                                   @else
                                        <input  value="1" name='is_public' type="checkbox" class="ace" />
                                   @endif
                                    
                                    <span class="lbl"> Is Public</span>
                                </label> &nbsp; &nbsp;
                                <label>
                                    @if ($post->is_published == 1 )
                                        <input  value="1" checked name='is_published' type="checkbox" class="ace" />
                                    @else
                                        <input   value="1" name='is_published' type="checkbox" class="ace" />
                                    @endif
                                    
                                    <span class="lbl"> Is Published</span>
                                </label> &nbsp; &nbsp;
                                <label>
                                    <input @if ($post->is_featured == 1 ) {{ 'checked' }} @endif  value="1" name='is_featured' type="checkbox" class="ace" />
                                    <span class="lbl"> Featured</span>
                                </label>
                            </div>
                        </div>



                    </div>
                    
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right">Attachments </label>
                        <div class="col-sm-10 ">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Name</td>
                                </tr>
                                @for ($i = 0; $i < count($postImages); $i++) @if ($postImages[$i][ 'type'] !='Image' ) 
                                    <tr id="imageRowId_{{$postImages[$i]['id']}}">
                                        <td>{{ $postImages[$i]['org_name'] }}</td>
                                       
                                    </tr>
                                @endif @endfor

                            </table>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right">Additional Fields</label>

                        <div class="col-sm-10" id='additional_key_value_id'>

                  
                            @for ($i = 0; $i < count($additionFields); $i++) <div class="form-group " id="key_value_row_id_{{$i+1}}">
                                <div class="col-sm-4 ">
                                    <input type="text" id="form_field_{{$i+1}}" value="{{ $additionFields[$i]['key'] }}" name="field_{{$i+1}}" placeholder="Field {{$i+1}}"
                                        class="form-control" />

                                </div>
                                <div class="col-sm-4 ">
                                    <input type="text" id="form_value_{{$i+1}}" value="{{ $additionFields[$i]['value'] }}" name="value_{{$i+1}}" placeholder="Value {{$i+1}}"
                                        class="form-control" />

                                </div>
                        </div>
                        @endfor
                      

                    </div>
                    <input type="hidden" id="additional_hidden_id" name="additional_hidden" value="{{ $arrayList }}" />

            </div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9 no-padding-left">
                    <a href="{{ URL::to('/posts') }}" class="btn btn-danger">
                                <i class="ace-icon fa fa-close bigger-110"></i>
                                Exit
                            </a>
                </div>
            </div>



            </form>

        </div>
        <div class="col-xs-2">

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
<!-- /.page-content -->
</div>
@endsection
 
@section('js')
<script src="{{ URL::asset('js/comboTreePlugin.js') }}"></script>
<script src="{{ URL::asset('js/summernote.js') }}"></script>
<script type="text/javascript">
    var catData =  {!! json_encode($categories) !!};
    var additionalIdList = [{{$arrayList}}];
    $( document ).ready(function() {

        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #postsMenuId").addClass("active");

        $('#desc-id').summernote({
            height: 200
        });
       

        
    });

 

  

</script>
@endsection