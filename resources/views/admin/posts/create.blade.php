@extends('layouts.default') 
@section('css')
{{--  <link rel="stylesheet" href="{{ URL::asset('css/summernote.css') }}" />  --}}
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('css/comboTreePlugin.css') }}" />
<style>
    .comboTreeDropDownContainer{
        z-index: 100;
    }
    .comboTreeArrowBtn{
        pointer-events: none !important;
        cursor: default !important;
    }

</style>
@endsection
 
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">Post</a>
            </li>
            <li class="active">Create new</li>
        </ul>
        <!-- /.breadcrumb -->

    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Create new post
                <!-- <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Common form elements and layouts
                </small> -->
            </h1>
        </div>
        <!-- /.page-header -->

        <div class="row">
            <div class="col-xs-10">
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                      @if(Session::has('alert-' . $msg))
                
                      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                      @endif
                    @endforeach
                </div>
                <form enctype="multipart/form-data" class="form-horizontal" role="form"  method="post" action="/posts">

                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Title <span class="red">*</span></label>
                        <div class="col-sm-10 {{ $errors->has('title') ? 'has-error' : '' }}">
                            <input type="text" id="form-field-1" value="{{ old('title') }}" name="title" placeholder="Title" class="form-control" />
                            @if ($errors->has('title'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('title') }} </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Description <span class="red">*</span></label>
                        <div class="col-sm-10  summernote {{ $errors->has('desc') ? 'has-error' : '' }}">
                            <textarea id="desc-id"  name="desc">{{ old('desc') }}</textarea>
                            @if ($errors->has('desc'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('desc') }} </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Category <span class="red">*</span></label>

                        <div class="col-sm-4 {{ $errors->has('category') ? 'has-error' : '' }} ">
                            
                            <input type="text" name="category"  class="form-control" id="categoryid" placeholder="Select an option" autocomplete='off'/>
                            @if ($errors->has('category'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('category') }} </div>
                            @endif
                        </div>
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Post Type </label>

                        <div class="select col-sm-4 {{ $errors->has('post_type') ? 'has-error' : '' }} ">
                            
                            <select class="form-control" name='post_type'  >
                                <option value="General">General</option>
                                <option value="Image">Image</option>
                                <option value="Media">Media</option>
                                <option value="File">File</option>
                            </select>
                            
                            @if ($errors->has('post_type'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('post_type') }} </div>
                            @endif
                        </div>                      
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Images </label>
                        <div class="col-sm-10 ">
                            {{--  <input type="text" id="form-field-1" value="{{ old('name') }}" name="title" placeholder="Image name" class="form-control" />  --}}
                            <input type="file" name="img_name[]" class="" id="id-input-file-2" multiple  />
                            @if (count($errors) > 0)                              
                                @for ($i = 0; $i < 25; $i++)
                                    @if ($errors->has('img_name.'.$i))
                                        <div class="help-block col-xs-12 col-sm-reset inline " style="color:#a94442"> 
                                            {{ $errors->first('img_name.'.$i) }} 
                                        </div>
                                    @endif
                                @endfor
                            @endif
                            {{--  @if ($errors->has('img_name'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> 
                                    {{ $errors->first('img_name') }} 
                                </div>
                            @endif  --}}
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Media </label>
                        <div class="col-sm-10 {{ $errors->has('media') ? 'has-error' : '' }}">
                            <input type="text" id="form-field-1" value="{{ old('media') }}" name="media" placeholder="Media" class="form-control" />
                            @if ($errors->has('media'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('media') }} </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Phone </label>

                        <div class="col-sm-4 {{ $errors->has('phone') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ old('phone') }}" name="phone" placeholder="Phone" class="form-control" />
                            @if ($errors->has('phone'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('phone') }} </div>
                            @endif
                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Email </label>

                        <div class="col-sm-4 {{ $errors->has('email') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ old('email') }}" name="email" placeholder="Email" class="form-control" />
                            @if ($errors->has('email'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('email') }} </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Latitude </label>

                        <div class="col-sm-4 {{ $errors->has('lati') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ old('lati') }}" name="lati" placeholder="Latitude" class="form-control" />
                            @if ($errors->has('lati'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('lati') }} </div>
                            @endif
                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1-1"> Longitude </label>

                        <div class="col-sm-4 {{ $errors->has('long') ? 'has-error' : '' }} ">
                            <input type="text" id="form-field-1" value="{{ old('long') }}" name="long" placeholder="Longitude" class="form-control" />
                            @if ($errors->has('long'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('long') }} </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" for="alive-date-id"> Alive Date </label>

                        <div class="col-sm-4 {{ $errors->has('alive_date') ? 'has-error' : '' }} ">
                           
                            <div class="input-group">
                                    <input class="form-control date-picker"  value="{{ old('alive_date') }}" name="alive_date" id="alive-date-id" type="text" data-date-format="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar bigger-110"></i>
                                    </span>
                                </div>
                            @if ($errors->has('alive_date'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('alive_date') }} </div>
                            @endif
                        </div>

                        <label class="col-sm-1 control-label no-padding-right" for="form-field-1-1">  </label>

                        <div class="col-sm-5 post ">
                            <div class="checkbox">
                                <label>
                                    <input  value="1" name='is_public' type="checkbox" class="ace" />
                                    <span class="lbl"> Is Public</span>
                                </label> &nbsp; &nbsp; &nbsp;
                                <label>
                                    <input   value="1" name='is_published' type="checkbox" class="ace" />
                                    <span class="lbl"> Is Published</span>
                                </label> &nbsp; &nbsp; &nbsp;
                                <label>
                                    <input  value="1" name='is_featured' type="checkbox" class="ace" />
                                    <span class="lbl"> Featured</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" >Attachments </label>
                        <div class="col-sm-10 {{ $errors->has('attachments') ? 'has-error' : '' }}">                            
                            <input type="file" name="attachments[]" class="" id="attachments_file" multiple />
                            
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-2 control-label no-padding-right" >Additional Fields</label>

                        <div class="col-sm-10" id='additional_key_value_id' >
                            <div class="form-group " id="key_value_row_id_1" >
                                <div class="col-sm-4 ">
                                    <input type="text" id="form_field_1" value="{{ old('field_1') }}" name="field_1" placeholder="Field 1" class="form-control" />
                                  
                                </div>
                                <div class="col-sm-4 ">
                                    <input type="text" id="form_value_1" value="{{ old('value_1') }}" name="value_1" placeholder="Value 1" class="form-control" />
                                  
                                </div>
                                <div class="col-sm-4 ">
                                        <i  class="fa fas fa-plus-circle fa-lg icon " id='plus_icon_1' onclick='plusIconClick(1)'></i>
                                        <i  class="fa fas fa-minus-circle fa-lg icon display-none " id='minus_icon_1' onclick='minusIconClick(1)'></i>
                                </div>
                            </div>
                            
                        </div>  
                        
                        <input type="hidden" id="additional_hidden_id" name="additional_hidden" value="1" />

                    </div>

                    
                    

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9 no-padding-left">
                            <button class="btn btn-primary" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Create
                            </button>
                            <a href="{{ URL::to('/posts') }}" class="btn btn-danger" >
                                <i class="ace-icon fa fa-close bigger-110"></i>
                                Exit
                            </a>
                        </div>
                    </div>



                </form>

            </div>
            <div class="col-xs-2">

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js')
<script src="{{ URL::asset('js/comboTreePlugin.js') }}"></script>
<script src="{{ URL::asset('js/summernote.js') }}"></script>
<script type="text/javascript">

    var catData =  {!! json_encode($categories) !!};
    var additionalIdList = [1];
    $( document ).ready(function() {
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #postsMenuId").addClass("active");

        $('#desc-id').summernote({
            height: 200
        });
 
        $('#id-input-file-2').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'
            ,
            preview_error : function(filename, error_code) {
            }
    
        }).on('change', function(){
        });

        

        
        
        
        $('#alive-date-id').datepicker({ dateFormat: 'dd-mm-yy' });

        $('#categoryid').comboTree({
			source : catData,
			isMultiple: true
        });
        
        


       

    });

    function plusIconClick(curId){
        var newId = curId+1; 

        additionalIdList.push(newId);
        $("#additional_hidden_id").val(additionalIdList.join(","));
        $("#minus_icon_"+curId).removeClass('display-none');
        $("#plus_icon_"+curId).addClass('display-none');

        $("#additional_key_value_id").append(
            '<div class="form-group " id="key_value_row_id_'+newId+'"  > '+
                    '<div class="col-sm-4 "> '+
                            '<input type="text" id="form_field_'+newId+'" value="" name="field_'+newId+'" placeholder="Field '+newId+'" class="form-control" /> '+
                      
                        '</div> '+
                    '<div class="col-sm-4 "> '+
                            '<input type="text" id="form_value_'+newId+'" value="" name="value_'+newId+'" placeholder="Value '+newId+'" class="form-control" /> '+
                      
                        '</div> '+
                    '<div class="col-sm-4 "> '+
                            '<i  class="fa fas fa-plus-circle fa-lg icon " id="plus_icon_'+newId+'" onclick="plusIconClick('+newId+')"></i> '+
                            '<i  class="fa fas fa-minus-circle fa-lg icon display-none " id="minus_icon_'+newId+'" onclick="minusIconClick('+newId+')"></i> '+
                            '</div>'+
                    '</div>'
        );
    }

    function minusIconClick(curId){
        if(confirm('Are you sure?')){
            var index = additionalIdList.indexOf(curId);
            additionalIdList.splice(index, 1);
            $("#additional_hidden_id").val(additionalIdList.join(","));
            $("#key_value_row_id_"+curId).remove();
        }
        
    }


    

   
</script>
@endsection