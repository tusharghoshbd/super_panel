@extends('layouts.default')

@section('css')
  
@endsection

@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">All Post</a>
            </li>
            <li class="active">List view</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">
        

        <div class="row">
            <div class="col-xs-12">
                
                <div class="row">
                    <div class="col-xs-12">
                         <h3 class="header smaller lighter blue">All Post</h3>
                         

                        {{--  <div class="clearfix">
                            <div class="pull-right tableTools-container"></div>
                        </div>   --}}
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))                
                                <p class="alert alert-{{ $msg }}">
                                    {{ Session::get('alert-' . $msg) }} 
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </p>
                                @endif
                            @endforeach
                        </div>
                            
                        <div class="table-header">
                            Post List
                            <a href="{{ URL::to('/posts/create') }}" class="btn btn-success pull-right" >
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Create
                            </a>
                            
                        </div>

                        <!-- div.table-responsive -->

                        <!-- div.dataTables_borderWrap -->
                        <div>
                            <table id="dynamic-table" class=" table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        {{--  <th>Phone</th>
                                        <th>Email</th>
                                        <th>Latitude</th>
                                        <th>longitude</th>  --}}
                                        <th>Featured</th>
                                        {{--  <th>Image</th>  --}}
                                        {{--  <th>Media</th>  --}}
                                        <th>Public </th>
                                        <th>Published</th>
                                        <th>Alive Date</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    
                                    @foreach($posts as $key => $post)
                                        <tr>    
                                            <td> <img alt="" height="30px" width="30px" src="{{$post->img_path}}"/></td>
                                            <td>                                                
                                                <a class="blue" href="{{ URL::to('/posts/'.$post->id) }}">{{$post->title}}</a>  
                                                {{--  {{$post->title}}  --}}
                                            </td>
                                            <td>{{$post->category_name}}</td>
                                            {{--  <td>{{$post->phone}}</td>
                                            <td>{{$post->email}}</td>
                                            <td>{{$post->lati}}</td>
                                            <td>{{$post->long}}</td>  --}}
                                            <td>
                                                @if ($post->is_featured == 1)
                                                    <i class="ace-icon fa fa-check bigger-130"></i>
                                                @endif                                                    
                                            </td>
                                            {{--  <td>
                                                <img alt="" height="20px" width="20px" src="uploads/posts/{{$post->img_name}}"/>
                                            </td>  --}}
                                            {{--  <td>{{$post->media_link}}</td>  --}}
                                            <td>
                                                @if ($post->is_public == 1)
                                                    <i class="ace-icon fa fa-check bigger-130"></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($post->is_published == 1)
                                                    <i class="ace-icon fa fa-check bigger-130"></i>
                                                @endif
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($post->alive_date)->format('d-m-Y')}}</td>
                                            <td>{{ \Carbon\Carbon::parse($post->created_at)->format('d-m-Y')}}</td>
                                            <td >
                                                <div > 
                                                    <a class="green" href="{{URL::to('/posts/'.$post->id.'/edit') }}">
                                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                    </a>   &nbsp;&nbsp;
    
                                                    <a class="red" href="javascript:void(0)"  onclick="event.preventDefault();
                                                             if(confirm('Are you sure?')){document.getElementById('delete-form_{{$post->id}}').submit();}">
                                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                    </a>
                                                    <form id="delete-form_{{$post->id}}" action="{{ route('posts.destroy', $post->id)}}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </div>
                                            </td>                 
                                        </tr>
                                    @endforeach

                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

               

                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div>

@endsection

@section('js')

<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #postsMenuId").addClass("active");

        $('#dynamic-table').DataTable({
            "ordering": false
        });
     });

</script>

@endsection