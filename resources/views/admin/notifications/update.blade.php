@extends('layouts.default') 
@section('css')
<link rel="stylesheet" href="{{ URL::asset('css/comboTreePlugin.css') }}" />
<style>
    .comboTreeDropDownContainer{
        z-index: 100;
    }
    .comboTreeArrowBtn{
        pointer-events: none !important;
        cursor: default !important;
    }

</style>
@endsection
    
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">Notification</a>
            </li>
            <li class="active">Update</li>
        </ul>
        <!-- /.breadcrumb -->

    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Update notification
            </h1>
        </div>
        <!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                      @if(Session::has('alert-' . $msg))
                
                      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                      @endif
                    @endforeach
                </div>

                <form  enctype="multipart/form-data" class="form-horizontal" role="form"  method="post" action="/notifications/{{$notification->id}}">

                    @method('PATCH')
                    @csrf
                    <div class="form-group ">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title <span class="red">*</span></label>

                        <div class="col-sm-6 {{ $errors->has('title') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" id="form-field-1" value="{{ $notification->title}}" name="title" placeholder="Title"  autocomplete='off' />
                            @if ($errors->has('title'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('title') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Select Type <span class="red">*</span> </label>

                        <div class="col-sm-6 {{ $errors->has('type') ? 'has-error' : '' }} ">
                            <select class="form-control" id="form-field-select-1" name="type">
                                <option value="">Select an option</option>
                                @foreach ($notificationTypes as $notificationType)
                                    <option value="{{ $notificationType->id}}" {{$notification->type_id == $notificationType->id? 'selected': ''}}>{{ $notificationType->name}}</option>
                                @endforeach
                            </select>
                           
                            @if ($errors->has('type'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('type') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title <span class="red">*</span></label>

                        <div class="col-sm-6 {{ $errors->has('body') ? 'has-error' : '' }}">
                            <textarea rows="3" cols="60" name="body">{{ $notification->body }}</textarea>                            
                            @if ($errors->has('body'))
                                <div class="help-block col-xs-12 col-sm-reset inline"> {{ $errors->first('body') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Add image </label>
                        <div class="col-sm-6 {{ $errors->has('img_name') ? 'has-error' : '' }}">
                            <input type="file" name="img_name" class="" id="id-input-file-2"  />
                            @if ($errors->has('img_name'))
                                <div class="help-block col-xs-12 col-sm-reset inline "> {{ $errors->first('img_name') }} </div>
                            @endif
                        </div>
                        <div class="col-sm-3"></div>
                        
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9 no-padding-left">
                            <button class="btn btn-primary" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Update
                            </button>
                            <a href="{{ URL::to('/notifications') }}" class="btn btn-danger" >
                                <i class="ace-icon fa fa-close bigger-110"></i>
                                Exit
                            </a>
                        </div>
                    </div>



                </form>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js')
<script src="{{ URL::asset('js/comboTreePlugin.js') }}"></script>
<script type="text/javascript">

    
    $( document ).ready(function() {
        
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #notificationsMenuId").addClass("active");

        $('#id-input-file-2').ace_file_input({
            no_file:'No File ...',
            btn_choose:'Choose',
            btn_change:'Change',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });


    });
</script>
@endsection