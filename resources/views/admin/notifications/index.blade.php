@extends('layouts.default') 
@section('css')

@endsection
 
@section('content')

<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>

            <li>
                <a href="#">Notifications</a>
            </li>
            <li class="active">List view</li>
        </ul>
        <!-- /.breadcrumb -->
    </div>

    <div class="page-content">


        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="header smaller lighter blue">Notification List</h3>


                        {{--
                        <div class="clearfix">
                            <div class="pull-right tableTools-container"></div>
                        </div> --}}
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">
                                        {{ Session::get('alert-' . $msg) }}
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    </p>
                                @endif 
                            @endforeach
                        </div>

                        <div class="table-header">
                            Notificatons {{--
                            <div class="row">
                                <div class="col-6">
                                    Results for "Latest Registered Domains"
                                </div>
                                <div class="col-6">

                                </div>
                            </div> --}}
                            <a href="{{ URL::to('/notifications/create') }}" class="btn btn-success pull-right">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Create
                            </a>

                        </div>
                        <div>
                        <table id="dynamic-table" class=" table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Title </th>
                                    <th>Type</th>
                                    <th>Body</th>
                                    <th>Image</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($notifications as $notification)
                                    <tr>
                                        <td>{{$notification->title}}</td>
                                        <td>{{$notification->type_name}}</td>
                                        <td>{{$notification->body}}</td>
                                        <td>
                                            @if( isset($notification->img_name ))
                                                <img alt="" height="20px" width="20px" src="uploads/notifications/{{$notification->img_name}}"/>
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($notification->created_at)->format('d-m-Y')}}</td>
                                        <td>
                                            <div> 
                                                {{-- <a class="green" href="{{URL::to('/notifications/'.$notification->id.'/edit') }}">
                                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                </a>                                     --}}

                                                <a class="red" href="javascript:void(0)"  onclick="event.preventDefault();
                                                         if(confirm('Are you sure?')){document.getElementById('delete-form_{{$notification->id}}').submit();}">
                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                </a>
                                                <form id="delete-form_{{$notification->id}}" action="{{ route('notifications.destroy', $notification->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    </div>
                </div>
                <!-- PAGE CONTENT ENDS -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
@endsection
 
@section('js') 

<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    
    $( document ).ready(function() {
        $(".custom-menu-item li").removeClass("active");
        $(".custom-menu-item #notificationsMenuId").addClass("active");

        $('#dynamic-table').DataTable({
            "ordering": false
        });

     });

</script>
@endsection