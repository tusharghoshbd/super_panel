<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('desc')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('lati')->nullable();
            $table->string('long')->nullable();
            $table->integer('is_featured')->nullable();
            $table->string('post_type')->nullable();
            $table->string('media_link')->nullable();
            $table->integer('is_public')->nullable();
            $table->integer('is_published')->nullable();
            $table->dateTime('alive_date')->nullable();
           
            $table->unsignedInteger('application_id');
            $table->foreign('application_id')->references('id')->on('applications');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->index('application_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
